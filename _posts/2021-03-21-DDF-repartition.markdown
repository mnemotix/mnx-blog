---
layout: post
title:  "Répartitions des mots localisés dans le Dictionnaire Des Francophones"
date:   2021-03-21 15:32:14 -0300
author: "Mnemotix"
categories: dataviz ddf lexicographie
---

Les données du DDF recèlent une richesse inédite. Dans ce billet, nous montrons un aperçu de ce qu'il est possible de tirer en terme de visualisation des données relatives à la localisation des définitions et des mots (les "formes" en terme lexicographique).

Les formes ou les définitions peuvent être associées à une localité dans le [DDF](https://www.dictionnairedesfrancophones.org/). La carte ci-dessous représente pour chaque localité du monde francophone (pays, région, ville, etc.) ayant au moins une forme associée, le nombre qui y sont rattachées. 

<iframe title="DDF - Répartition géographique des mots localisés" aria-label="Map" id="datawrapper-chart-4SxRV" src="https://datawrapper.dwcdn.net/4SxRV/3/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="365"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(a){if(void 0!==a.data["datawrapper-height"])for(var e in a.data["datawrapper-height"]){var t=document.getElementById("datawrapper-chart-"+e)||document.querySelector("iframe[src*='"+e+"']");t&&(t.style.height=a.data["datawrapper-height"][e]+"px")}}))}();
</script>
