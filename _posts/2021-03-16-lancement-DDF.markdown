---
layout: post
title:  "Lancement du Dictionnaire Des Francophones"
date:   2021-03-16 09:00 +0100
author: "Mnemotix"
categories: ddf dictionnaire opendata francophone
background: '/assets/img/ddf-back.png'
---

Nous sommes particulièrement fiers d'annoncer le lancement du [Dictionnaire des francophones](https://www.dictionnairedesfrancophones.org/), premier dictionnaire francophone en ligne agrégeant des sources lexicographiques existantes et ouverts à toutes les contributions. C'est un projet institutionnel novateur qui présente d'une part une partie de consultation au sein de laquelle sont compilées plusieurs ressources lexicographiques (avec plus de 440 000 mots et 610 000 définitions) dont le Wiktionnaire, le Grand Dictionnaire Terminlogique Québecquois, l'Inventaire des particularité du français en Afrique de l'Ouest, etc.; et d'autre part une partie collaborative pour développer les mots et faire vivre la langue française. 

<img alt="Application DDF" src="/assets/img/ddf-tab-text.png" style="width:100%"/>

Répondant à un appel d'offre initié par l'institut 2IF et le Ministère de la Culture, Mnémotix a pris en main dès le début du projet la conception et le développement de l'architecture dédiée à l'alignement et l'intégration des données sources. Comme dans la plupart de nos projet, le DDF s'appuie sur notre middleware sémantique Synaptix pour permettre à la fois un stockage sous forme de graphe de connaissance et un accès très rapide aux données afin d'être capable d'absorber des gros pics de consultations typiques d'une application grand public --et qui plus est internationale-- comme le DDF.



L'import de certaines ressources comme l'Inventaire ou le Wiktionnaire ont constitué de véritables challenges en matière d'intégration de données de par le manque quasi totale de structure de leur source (documents PDF pour l'Inventaire, ou wikitext pour le wiktionnaire). Grâce à notre expérience et à la rigueur du modèle pivot (co-conçue avec la maîtrise d'ouvrage), le résultat propose une expérience de consultation de dictionnaire passionnante avec une mise en exergue de la provenance de certains mots ou des différences notoires de sens entre les différentes parties du monde francophone.



En guise de conclusion nous vous invitons à visionner la vidéo de présentation et à explorer la richesse du DDF, pourquoi pas en découvrant tous les sens possibles du mot [canne](ddf-tab-text.png) ou encore en apprenant ce que veut dire [techniquer](https://www.dictionnairedesfrancophones.org/form/techniquer) un de bientôt nombreux mots introuvables dans les autres dictionnaires francophones.

<iframe width="100%" height="480px" src="https://www.youtube.com/embed/kDEsFJvJkpk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Références**
* L'application DDF est accessible à [https://www.dictionnairedesfrancophones.org](https://www.dictionnairedesfrancophones.org/)
* Page présentant le [lancement du DDF](https://www.culture.gouv.fr/Actualites/Suivez-en-direct-le-lancement-du-Dictionnaire-des-francophones) sur le site du Ministère de la Culture



