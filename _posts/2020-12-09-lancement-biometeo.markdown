---
layout: post
title:  "Lancement de l'application Biométéo"
date:   2020-12-09 14:00:00 -0300
categories: biodiversité biometeo dordogne application  
background: '/assets/img/mnb-back.png'
author: "Mnemotix"
---

Nous sommes très heureux d'annoncer le lancement officiel ce mercredi 9 décembre 2020 de [l'application Biométéo](https://biometeo.dordogne.fr/) réalisée par Mnémotix [pour le compte du Conseil Départemental de Dordogne](https://www.dordogne.fr/information-transversale/actualites/default-f77d398a3e-22330562). 
Le concept de cette application web compatible mobile, ouverte et gratuite, consiste à croiser des données de météo, de biodiversité, et des données liées aux rivières et nappes phréatiques. L'objectif de ce projet est de susciter la curiosité du public quant au lien entre le vivant et son environnement à travers l'usage d'une application utile au quotidien.
L'usager choisi d'abord sa localisation, et l'application présente alors le détail des prévisions météo pour sa commune, ainsi qu'une sélection de 9 espèces (animal ou végétale) ayant été observées dans le département de Dordogne, et les courbes de  l'évolution sur le mois écoulé des niveaux de la nappe phréatique et de la rivière les plus proches. Il est possible de plus de partager le lien vers la fiche d'une espèce, ou de l'enregistrer comme favori. 

<img alt="biométéo portable " src="/assets/img/portable.png" style="width:100%"/>

La stratégie de médiation scientifique adoptée par la Biométéo de Dordogne est de donner accès à des données spécifiques et précises de manière accessible à travers une mise en forme simple et ergonomique.De multiples liens renvoient aux sources et à d'avantage de détails pour chaque espèce présentée, ou pour les données relatives aux rivières et nappes phréatiques. Ainsi l'application a pour vocation de sensibiliser le public, et de créer l'envie d'en apprendre plus. 

Le coeur de cette application grand public est constitué par un hub de données développé par Mnémotix et basé sur le middleware Synaptix (brique commune à tous nos projets d'intégration de données).  Ce hub collecte à la fois des données froides (évoluant peu dans le temps) sous forme d'imports ponctuels, et des données dites "chaudes" (car évoluant en permance) grâce à des connecteurs dédiés et procédant à des collectes automatiques. Ces données hétérogènes sont rendues interopérables en étant traduites dans un modèle "pivot" utilisant au maximum les standards existants.  Toutes les données sémantisées sont par ailleurs disponibles sur un endpoint [SPARQL](https://cg24.gitlab.io/mnb-repository/doc/hubdedonnees.html), via l'[API GraphQL](https://biometeo.dordogne.fr/graphql/playground), et via la plateforme d'[open data du département de Dordogne](https://data.dordogne.fr/search?q=biometeo) pour une partie des données froides. L'architecture de ce hub de données est décrite dans l'image ci-après. 

<img alt="biométéo portable " src="/assets/img/archi-mnb.png" style="width:100%"/>

Plus précisément les sources de données exploitées dans ce projet sont:
* Données météo: [https://openweathermap.org](https://openweathermap.org)
* Données d’indice de qualité de l’air : Atmo Nouvelle-Aquitaine ([https://www.atmo-nouvelleaquitaine.org/](https://www.atmo-nouvelleaquitaine.org/))
* Données taxonomiques sur les espèces: Taxref, produit par le Muséum National d’Histoire Naturelle (MNHN) (UMS 2006 Patrimoine naturel) qui met ainsi à disposition toutes les données relatives aux espèces recensées en France (description taxonomique (rang, ordre, classe, famille, etc.), noms scientifiques et vernaculaires, habitats. Ces données sont rendues disponibles via l’API Taxref en version web et Linked Open Data ( ersion [Web](https://taxref.mnhn.fr/taxref-web) et [Linked Data](https://github.com/frmichel/taxref-ld))
* Données d’observation d’espèces : Le site BioMétéo exploite également des données d’occurrence de taxons provenant du consortium international GBIF ([Global Biodiversity Information System](https://www.gbif.org/)). Ces données d’occurrence attestent de l’observation d’un organisme en un lieu donné, à un moment donné. Ces données sont fournies au GBIF par un ensemble d’acteurs et de dispositifs. Chaque donnée d’occurrence est rattachée à un jeu de données. Les jeux de données exploitées pour la BioMétéo sont listés sur [cette page](https://www.gbif.org/occurrence/download/0065750-200221144449610#datasets).
* Données de niveau des nappes phréatiques et de débit des rivières: Nous collectons aussi quotidiennement les relevés de niveau des nappes phréatiques et de débit des rivières disponibles sur l’API Hubeau ([hubeau.eaufrance.fr](https://hubeau.eaufrance.fr/)) pour les stations actives situées en Dordogne. Les débits des rivières sont donnés par l’[API Hydrométrie](https://hubeau.eaufrance.fr/page/api-hydrometrie), et les niveaux des nappes phréatiques par [l’API Piézométrie](https://hubeau.eaufrance.fr/page/api-piezometrie).
 
**Références**
* La [documentation de l'application](https://cg24.gitlab.io/mnb-repository/) vous donne les clés pour accéder aux données du Hub et les exploiter
* La [page du projet Maison Numérique de la Biodiversité](https://www.dordogne.fr/relever-les-defis-du-21e-siecle/lexcellence-environnementale/biodiversite/maison-numerique-de-la-biodiversite-une-boite-a-outils-numeriques-au-service-de-lexcellence-environnementale) sur le site du CD Dordogne
* Le [dossier de presse](https://www.dordogne.fr/fileadmin/Espace_presse/Dossiers_de_presse/20201209_DP_Biometeo_BD.pdf)
* [Article sur le média Curieux ](https://www.curieux.live/2020/12/09/dordogne-lapplication-biometeo-sensibilise-le-public-a-la-biodiversite-locale/)
* [Article sur France Bleu](https://www.francebleu.fr/infos/environnement/a-teste-pour-vous-un-nouveau-site-pour-decouvrir-chaque-jour-la-biodiversite-de-dordogne-1607535512)